function pairs(obj) {
    const pairsArray = [];
    for (const key in obj) {
        pairsArray.push([key, obj[key]]);
    }

    return pairsArray;
}


module.exports = pairs;

