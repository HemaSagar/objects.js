function keys(obj) {
    const objKeys = [];

    for (const key in obj) {
        objKeys.push(key);

    }
    
    return objKeys;

}

module.exports = keys;
