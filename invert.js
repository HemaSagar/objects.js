function invert(obj) {
    const invertObj = {};

    //making the key as value and value as key into new object
    for (const key in obj) {
        invertObj[obj[key]] = key;
    }

    return invertObj;
}


module.exports = invert;