function mapObject(obj, cb) {
    modifiedObj = {};

    for (const key in obj) {
        modifiedObj[key] = cb(obj[key], key, obj);

    }

    return modifiedObj;
}

module.exports = mapObject;