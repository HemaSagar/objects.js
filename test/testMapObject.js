const mapObject = require('../mapObject')

//Given object for testing
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const modifiedObj = mapObject(testObject,(value,key,obj) => {
    return "value changed from mapObject";

});

console.log(modifiedObj);
