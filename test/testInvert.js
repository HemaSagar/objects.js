const invert = require('../invert.js')

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const invertObj = invert(testObject);

console.log(invertObj);