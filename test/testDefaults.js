const defaults = require('../defaults')

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const newObj = defaults(testObject, {gender: 'Male'});

console.log(newObj);