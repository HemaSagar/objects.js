function values(obj) {
    const objValues = [];

    for (const key in obj) {
        if (obj[key] instanceof Function) {
            continue;
        }

        objValues.push(obj[key]);
    }

    return objValues;
}

module.exports = values;